# -*- coding: utf-8 -*-
from contextlib import contextmanager

from pjtl.db.utils import name_for_scalar_relationship, name_for_collection_relationship
from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# MAIN_DATABASE_MODEL_MAP = {"tArtikelHistory": "ArticleHistory"}
# MAIN_DATABASE_MODEL_MAP = {}


class JtlDB(object):
    def __init__(self, host, username, password, db_name, port=None, model_map=None):

        self._host = host
        self._username = username
        self._password = password
        self._db_name = db_name
        if port is None:
            port = 50815
        self._port = port

        self._db_engine = None
        self._db_sessionmaker = None
        self._models_created = False

        if model_map is None:
            model_map = {}
        self._model_map = model_map

        self._base = None

    @property
    def db_engine(self):

        if self._db_engine is None:
            self._db_engine = create_engine(self.connection_string)
        return self._db_engine

    @property
    def db_sessionmaker(self):

        if self._db_sessionmaker is None:
            self._db_sessionmaker = sessionmaker(bind=self.db_engine)

        return self._db_sessionmaker

    @contextmanager
    def session_scope(self):
        """Provide a transactional scope around a series of operations."""

        session = self.db_sessionmaker()
        try:
            yield session
            session.commit()
        except (Exception):
            session.rollback()
            raise
        finally:
            session.close()

    @property
    def base(self):

        if self._base is not None:
            return self._base

        metadata = MetaData(self.db_engine)

        metadata.reflect(bind=self.db_engine, only=self._model_map.keys())
        Model = declarative_base(metadata=metadata, bind=self.db_engine)
        self._base = automap_base(metadata=metadata, declarative_base=Model)
        self._base.prepare(
            name_for_scalar_relationship=name_for_scalar_relationship,
            name_for_collection_relationship=name_for_collection_relationship,
        )
        for cls in self._base.classes:
            cls.__table__.info = {"bind_key": "main"}
            if cls.__table__.name in self._model_map:
                globals()[self._model_map[cls.__table__.name]] = cls

        self._models_created = True
        return self._base

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def username(self):
        return self._username

    @property
    def password(self):
        return self._password

    @property
    def db_name(self):
        return self._db_name

    @property
    def connection_string(self):
        return "mssql+pymssql://{}:{}@{}:{}/{}?charset=utf8".format(
            self._username, self._password, self._host, self._port, self._db_name
        )
