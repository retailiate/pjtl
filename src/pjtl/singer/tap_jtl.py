# -*- coding: utf-8 -*-
import json
import os
import sys

import click

from pjtl.singer import SingerStreams


@click.command()
@click.option("--config", help="configuration file", required=False)
@click.option("--discover", is_flag=True, help="enable discovery mode", default=False)
@click.option("--state", help="state file", required=False)
@click.option("--catalog", help="catalog file", required=False)
@click.option("--stream", help="filter a single stream", required=False)
@click.pass_context
def cli(ctx, config, discover, state, catalog, stream):

    if not config:
        click.echo("No configuration file provided.")
        sys.exit(1)

    state_dict = {}
    if state:
        state = os.path.expanduser(state)
        if os.path.exists(state):
            with open(state, "r") as s:
                lines = s.readlines()

            for line in reversed(lines):

                try:
                    state_data = json.loads(line)
                    bookmarks = state_data.get("bookmarks", None)
                    if bookmarks is None:
                        continue

                    state_dict = state_data
                    break
                except (Exception):
                    pass

    config = os.path.expanduser(config)
    if not os.path.isfile(config):
        click.echo("Could not find config file: {}".format(config))
        sys.exit(1)

    with open(config, "r") as c:
        config_data = json.load(c)

    host = config_data["host"]
    username = config_data["username"]
    password = config_data["password"]
    db_name = config_data["db_name"]
    port = config_data.get("port", None)
    start_date = config_data.get("start_date", None)

    db_details = dict(
        host=host, username=username, password=password, db_name=db_name, port=port
    )

    streams = SingerStreams(db_details=db_details, catalog_path=catalog, filter=stream)

    if discover:
        streams.write_catalog()
        sys.exit()

    streams.load_data(state=state_dict, start_date=start_date)

    #  from .db import ArticleHistory
    #
    # window_size = 10000
    # with db.session_scope() as session:
    #
    #     if last_orig_id is None:
    #         q = session.query(ArticleHistory).filter(
    #             ArticleHistory.dGebucht >= start_date
    #         )
    #     else:
    #         q = (
    #             session.query(ArticleHistory)
    #             .filter(ArticleHistory.kArtikelHistory > last_orig_id)
    #             .filter(ArticleHistory.dGebucht >= start_date)
    #         )
    #     i = 0
    #     ah = None
    #     for ah, now in windowed_query(q, ArticleHistory.kArtikelHistory, window_size):
    #         i = i + 1
    #
    #         gebucht = pytz.utc.localize(ah.dGebucht).isoformat()
    #         record = {"dGebucht": gebucht}
    #         for k, v in article_history_map.items():
    #             if k == "dGebucht":
    #                 continue
    #             record[k] = getattr(ah, k)
    #         singer.write_record(
    #             stream_name="tArtikelHistory", record=record, time_extracted=now
    #         )
    #         # if i >= window_size:
    #         singer.write_state(ah.kArtikelHistory)
    #         i = 0
    #
    #     if ah is not None:
    #         singer.write_state(ah.kArtikelHistory)
    #     else:
    #         singer.write_state(last_orig_id)
