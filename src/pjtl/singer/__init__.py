# -*- coding: utf-8 -*-
import importlib
import os

import pytz
import singer
from singer import Catalog, write_bookmark, get_bookmark
from singer.catalog import write_catalog
from singer.metadata import to_map

from pjtl.db import JtlDB
from pjtl.db.utils import windowed_query


class SingerStreams(object):
    def __init__(self, db_details, catalog_path=None, filter=None):

        if catalog_path is None:
            catalog_path = os.path.join(
                os.path.dirname(__file__), "default_catalog.json"
            )

        self._catalog = Catalog.load(catalog_path)
        self._streams = {}
        self._filter = filter
        for cat_entry in self._catalog.streams:
            stream = SingerStream(cat_entry)
            if self._filter:
                if self._filter not in stream.stream_name:
                    continue
            self._streams[stream.stream_name] = stream

        self._model_map = {}
        for stream in self._streams.values():
            self._model_map[stream.table_name] = stream.model_class_name

        self._db_details = db_details
        self._db = None
        self._models_loaded = False

    def load_models(self):
        # load models
        self.db.base

        mdl = importlib.import_module("pjtl.db")
        for stream in self._streams.values():
            stream.model_class = getattr(mdl, stream.model_class_name)
            stream.db = self._db

        self._models_loaded = True

    # def create_catalog(self):
    #     streams = []
    #     for stream in self._streams.values():
    #         entry = CatalogEntry()
    #         entry.tap_stream_id = stream.stream_id
    #         entry.stream = stream.stream_name
    #         # entry.replication_key = stream.get('replication_key')
    #         entry.key_properties = stream.key_properties
    #         # entry.database = stream.get('database_name')
    #         entry.table = stream.table_name
    #         entry.schema = Schema.from_dict(stream.schema)
    #         # entry.is_view = stream.get('is_view')
    #         # entry.stream_alias = stream.get('stream_alias')
    #         # entry.metadata = stream.get('metadata')
    #         # entry.replication_method = stream.get('replication_method')
    #         streams.append(entry)
    #
    #     return Catalog(streams)

    def write_catalog(self):
        catalog_path = os.path.join(os.path.dirname(__file__), "default_catalog.json")
        write_catalog(Catalog.load(catalog_path))

    def get(self, stream_name):

        return self._streams.get(stream_name, None)

    @property
    def db(self):
        if self._db is not None:
            return self._db

        self._db = JtlDB(
            host=self._db_details["host"],
            username=self._db_details["username"],
            password=self._db_details["password"],
            db_name=self._db_details["db_name"],
            port=self._db_details["port"],
            model_map=self._model_map,
        )
        return self._db

    def load_data(self, state, *stream_names, start_date=None):

        if not self._models_loaded:
            self.load_models()

        if not stream_names:
            stream_names = self._streams.keys()

        for stream_name in stream_names:
            stream = self._streams[stream_name]
            stream.load_data(state=state, start_date=start_date)


class SingerStream(object):
    def __init__(self, catalog_entry):

        self._catalog_entry = catalog_entry
        self._metadata = to_map(self._catalog_entry.to_dict().get("metadata", []))
        self._stream_metadata = self._metadata[()]
        self._model_class = None
        self._db = None

    @property
    def table_name(self):
        return self._catalog_entry.table

    @property
    def properties(self):
        return self.schema.get("properties", {})

    @property
    def schema(self):
        return self._catalog_entry.schema.to_dict()

    @property
    def replication_key(self):
        return self._stream_metadata["replication_key"]

    @property
    def timestamp_key(self):
        return self._stream_metadata.get("timestamp_key", None)

    @property
    def key_properties(self):
        other_keys = self._stream_metadata.get("table_key_properties", None)
        if other_keys:
            if self.replication_key in other_keys:
                return other_keys
            else:
                return [self.replication_key] + other_keys
        else:
            return [self.replication_key]

    @property
    def model_class_name(self):
        return self._stream_metadata.get("model_class_name", self.table_name)

    @property
    def stream_name(self):
        return self._catalog_entry.stream

    @property
    def stream_id(self):
        return self._catalog_entry.tap_stream_id

    @property
    def default_last_id(self):
        return self._stream_metadata.get("default_last_id", None)

    @property
    def default_start_date(self):
        start_date = self._stream_metadata.get("default_start_date", None)
        if start_date is None:
            start_date = "1970-01-01T00:00:00.0Z"

        return start_date

    @property
    def singer_schema(self):
        return {
            "stream_name": self.stream_name,
            "schema": self.properties,
            "key_properties": self.key_properties,
        }

    @property
    def model_class(self):
        if self._model_class is None:
            raise Exception(
                "Model class not set yet for table: {}".format(self.get_table_name())
            )
        return self._model_class

    @model_class.setter
    def model_class(self, model_class):

        self._model_class = model_class

    @property
    def db(self):
        if self._db is None:
            raise Exception("db not set yet for table: {}".format(self.table_name))
        return self._db

    @db.setter
    def db(self, db):
        self._db = db

    def write_schema(self):

        singer.write_schema(
            stream_name=self.stream_name,
            schema=self.schema,
            key_properties=self.key_properties,
        )

    def write_state(self, state, last_row):

        if last_row is not None:
            id_value = getattr(last_row, self.replication_key)
            write_bookmark(
                state=state,
                tap_stream_id=self.stream_id,
                key=self.replication_key,
                val=id_value,
            )

        singer.write_state(state)

    def load_data(self, state, start_date=None):

        if start_date is None:
            start_date = self.default_start_date

        last_orig_id = get_bookmark(
            state=state,
            tap_stream_id=self.stream_id,
            key=self.replication_key,
            default=None,
        )

        if last_orig_id is None:
            last_orig_id = self.default_last_id

        self.write_schema()

        window_size = 10000
        with self.db.session_scope() as session:

            id_key_attr = getattr(self.model_class, self.replication_key)
            if self.timestamp_key:
                timestamp_attr = getattr(self.model_class, self.timestamp_key)
            else:
                timestamp_attr = None

            if last_orig_id is None:
                if timestamp_attr is None:
                    q = session.query(self.model_class)
                else:
                    q = session.query(self.model_class).filter(
                        timestamp_attr >= start_date
                    )
            else:
                if timestamp_attr is None:
                    q = session.query(self.model_class).filter(
                        id_key_attr > last_orig_id
                    )
                else:
                    q = (
                        session.query(self.model_class)
                        .filter(id_key_attr > last_orig_id)
                        .filter(timestamp_attr >= start_date)
                    )

            i = 0
            row = None
            for row, now in windowed_query(q, id_key_attr, window_size):
                i = i + 1

                record = {}
                for k, v in self.properties.items():
                    if v.get("format", None) == "date-time":
                        cell = getattr(row, k)
                        if not cell:
                            record[k] = cell
                        else:
                            record[k] = pytz.utc.localize(getattr(row, k)).isoformat()
                    else:
                        print(type(row))
                        print(row)
                        record[k] = getattr(row, k)
                singer.write_record(
                    stream_name=self.stream_name, record=record, time_extracted=now
                )

                if i >= window_size:
                    # print("WRITING STATE: {} - {}".format(i, state))
                    self.write_state(state=state, last_row=row)
                    i = 0

            # print("WRITING STATE: FINAL - {}".format(state))
            self.write_state(state=state, last_row=row)

            # for row, now in windowed_query(q, self.model_class.kArtikelHistory, window_size):
            #     i = i + 1
            #
            #     # gebucht = pytz.utc.localize(row.dGebucht).isoformat()
            #     # record = {"dGebucht": gebucht}
            #     record = {}
            #     for k, v in self.properties.items():
            #         # if k == "dGebucht":
            #         #     continue
            #         record[k] = getattr(row, k)
            #     singer.write_record(
            #         stream_name=self.stream_name, record=record, time_extracted=now
            #     )
            #     # if i >= window_size:
            #     singer.write_state(row.kArtikelHistory)
            #     i = 0
            #
            # if row is not None:
            #     singer.write_state(row.kArtikelHistory)
            # else:
            #     singer.write_state(last_orig_id)
