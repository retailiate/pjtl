[![PyPI status](https://img.shields.io/pypi/status/pjtl.svg)](https://pypi.python.org/pypi/pjtl/)
[![PyPI version](https://img.shields.io/pypi/v/pjtl.svg)](https://pypi.python.org/pypi/pjtl/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/pjtl.svg)](https://pypi.python.org/pypi/pjtl/)
[![Pipeline status](https://gitlab.com/frkl/pjtl/badges/develop/pipeline.svg)](https://gitlab.com/frkl/pjtl/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# pjtl

*Python library to interact with JTL WaWi*


## Description

Documentation still to be done.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'pjtl' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 pjtl
    git clone https://gitlab.com/frkl/pjtl
    cd <pjtl_dir>
    pyenv local pjtl
    pip install -e .[develop,testing,docs]
    pre-commit install
    
    
# Tasks

JTL->POSTGRES 

Tabellendefinitionen werden ../projects/retailiate/pjtl/dev_catalog.json festgelegt 
Nur für's Development

Das bedeutet - jede Tabelle nur 1 x anlegen
Nur Daten ziehen die benötigt werden

Test in ../projects/retailiate/pjtl/dev_catalog.json
Dann git commit in Pycharm / Git Push
Argo macht den Rest - dann ist ../projects/retailiate/pjtl/dev_catalog.json aktuell -> Ist im Moment unsere Production Umgebung

 



## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
