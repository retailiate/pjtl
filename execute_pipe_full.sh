#!/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "${THIS_DIR}"
state_arg=""
if [[ -f "state.json" ]]; then
    if [[ -s "state.json" ]]; then
        state_arg="--state state.json"
    else
        # delete empty state file
        rm "state.json"
    fi
fi
./tap-jtl --config tap_config.json ${state_arg} | ./target-postgres --config target_config.json >> state.json
if [[ -f "state.json" ]]; then
    tail -1 state.json > state.json.tmp && mv state.json.tmp state.json
fi
